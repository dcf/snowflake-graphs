PYTHON = python3

# https://metrics.torproject.org/collector/archive/bridge-descriptors/extra-infos/
BRIDGE_EXTRA_INFOS = $(wildcard bridge-extra-infos-*.tar.xz)
# https://metrics.torproject.org/collector/archive/snowflakes/
SNOWFLAKES = $(wildcard snowflakes-*.tar.xz)
.SECONDARY: $(BRIDGE_EXTRA_INFOS) $(SNOWFLAKES)

# Names of descriptor files that are known to contain syntax errors.
# https://gitlab.torproject.org/tpo/network-health/metrics/collector/-/issues/40045
SNOWFLAKE_STATS_IGNORE = snowflakes-2024-08/22/2024-08-22-22-44-14-snowflake-stats

all: \
	userstats-bridge-transport-multi.csv \
	userstats-bridge-combined-multi.csv \
	bandwidth-multi.csv \
	proxy-country.csv \
	proxy-type.csv \
	proxy-nat-type.csv \
	client-match.csv
.PHONY: all

userstats-bridge-transport-multi.csv: .EXTRA_PREREQS = userstats-bridge-transport-multi common.py
userstats-bridge-transport-multi.csv: \
	$(addprefix dir_reqs/,$(BRIDGE_EXTRA_INFOS:.tar.xz=.dir_reqs.csv)) \
	$(addprefix transport_ips/,$(BRIDGE_EXTRA_INFOS:.tar.xz=.transport_ips.csv))
	$(PYTHON) userstats-bridge-transport-multi $^ > "$@"

userstats-bridge-combined-multi.csv: .EXTRA_PREREQS = userstats-bridge-combined-multi common.py
userstats-bridge-combined-multi.csv: \
	$(addprefix dir_reqs/,$(BRIDGE_EXTRA_INFOS:.tar.xz=.dir_reqs.csv)) \
	$(addprefix country_reqs/,$(BRIDGE_EXTRA_INFOS:.tar.xz=.country_reqs.csv)) \
	$(addprefix transport_ips/,$(BRIDGE_EXTRA_INFOS:.tar.xz=.transport_ips.csv))
	$(PYTHON) userstats-bridge-combined-multi $^ > "$@"

bandwidth-multi.csv: .EXTRA_PREREQS = bandwidth-multi common.py
bandwidth-multi.csv: \
	$(addprefix bw_history/,$(BRIDGE_EXTRA_INFOS:.tar.xz=.bw_history.csv))
	$(PYTHON) bandwidth-multi $^ > "$@"

proxy-country.csv: .EXTRA_PREREQS = proxy-country common.py
proxy-country.csv: \
	$(addprefix proxy_country/,$(SNOWFLAKES:.tar.xz=.proxy_country.csv))
	$(PYTHON) proxy-country $^ > "$@"

proxy-type.csv: .EXTRA_PREREQS = proxy-type common.py
proxy-type.csv: \
	$(addprefix proxy_type/,$(SNOWFLAKES:.tar.xz=.proxy_type.csv))
	$(PYTHON) proxy-type $^ > "$@"

proxy-nat-type.csv: .EXTRA_PREREQS = proxy-nat-type common.py
proxy-nat-type.csv: \
	$(addprefix proxy_nat_type/,$(SNOWFLAKES:.tar.xz=.proxy_nat_type.csv))
	$(PYTHON) proxy-nat-type $^ > "$@"

client-match.csv: .EXTRA_PREREQS = client-match common.py
client-match.csv: \
	$(addprefix client_match/,$(SNOWFLAKES:.tar.xz=.client_match.csv))
	$(PYTHON) client-match $^ > "$@"

dir_reqs/bridge-extra-infos-*.dir_reqs.csv \
country_reqs/bridge-extra-infos-*.country_reqs.csv \
transport_ips/bridge-extra-infos-*.transport_ips.csv \
bw_history/bridge-extra-infos-*.bw_history.csv \
: .EXTRA_PREREQS = bridge-extra-info
dir_reqs/bridge-extra-infos-%.dir_reqs.csv \
country_reqs/bridge-extra-infos-%.country_reqs.csv \
transport_ips/bridge-extra-infos-%.transport_ips.csv \
bw_history/bridge-extra-infos-%.bw_history.csv \
&: bridge-extra-infos-%.tar.xz
	@mkdir -p dir_reqs country_reqs transport_ips bw_history
	$(PYTHON) bridge-extra-info "$<" "dir_reqs/bridge-extra-infos-$*.dir_reqs.csv" "country_reqs/bridge-extra-infos-$*.country_reqs.csv" "transport_ips/bridge-extra-infos-$*.transport_ips.csv" "bw_history/bridge-extra-infos-$*.bw_history.csv"

proxy_country/snowflakes-*.proxy_country.csv \
proxy_type/snowflakes-*.proxy_type.csv \
proxy_nat_type/snowflakes-*.proxy_nat_type.csv \
client_match/snowflakes-*.client_match.csv \
: .EXTRA_PREREQS = snowflake-stats
proxy_country/snowflakes-%.proxy_country.csv \
proxy_type/snowflakes-%.proxy_type.csv \
proxy_nat_type/snowflakes-%.proxy_nat_type.csv \
client_match/snowflakes-%.client_match.csv \
&: snowflakes-%.tar.xz
	@mkdir -p proxy_country proxy_type proxy_nat_type client_match
	$(PYTHON) snowflake-stats $(addprefix --ignore ,$(SNOWFLAKE_STATS_IGNORE)) "$<" "proxy_country/snowflakes-$*.proxy_country.csv" "proxy_type/snowflakes-$*.proxy_type.csv" "proxy_nat_type/snowflakes-$*.proxy_nat_type.csv" "client_match/snowflakes-$*.client_match.csv"

# Disable implicit rules for executable programs that appear in
# .EXTRA_PREREQS.
bridge-extra-info \
userstats-bridge-transport-multi \
userstats-bridge-combined-multi \
bandwidth-multi \
snowflake-stats \
proxy-country \
proxy-type \
proxy-nat-type \
: ;

.DELETE_ON_ERROR:
