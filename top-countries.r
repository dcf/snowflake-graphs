# Makes a graph showing the TOP_N countries with the most Snowflake users per
# day.
#
# Usage:
#   Rscript top-countries.r userstats-bridge-combined-multi.csv snowflake-top-countries.png

options(tidyverse.quiet = TRUE)
library("tidyverse")

# DATE_LIMITS is defined in common.r.
source("common.r")

TOP_N <- 5

LINE_SIZE <- 0.2

(function() {
	args <- commandArgs(trailingOnly = TRUE)
	if (length(args) != 2) {
		stop("usage: Rscript top-countries.r userstats-bridge-combined-multi.csv snowflake-top-countries.png")
	}
	userstats_bridge_combined_csv_path <<- args[[1]]
	output_path <<- args[[2]]
})()

bridge_combined <- read_csv(userstats_bridge_combined_csv_path,
		comment = "#",
		col_types = cols(
			date          = col_date(),
			fingerprint   = col_factor(),
			country       = col_factor(),
			transport     = col_factor(),
			low           = col_double(),
			high          = col_double(),
			num_instances = col_double(),
			coverage      = col_double(),
		)
	) %>%

	filter(transport == "snowflake" & fingerprint %in% names(WANTED_FINGERPRINTS)) %>%

	# Compensate for days when not all descriptors were published.
	mutate(across(c(low, high), ~ .x / (coverage / pmax(num_instances, coverage))))

# Comment to show all bridges, not only snowflake-01.
bridge_combined <- filter(bridge_combined, fingerprint == "5481936581E23D2D178105D44DB6915AB06BFB7F")

top_countries <- bridge_combined %>%
	group_by(date, fingerprint, transport) %>%
	slice_max((low + high) / 2, n = TOP_N) %>%
	ungroup()

# Drop country factors that aren't in the displayed part of the graph.
top_countries <- top_countries %>%
	filter(lubridate::`%within%`(date, do.call(lubridate::interval, as.list(DATE_LIMITS)))) %>%
	mutate(country = fct_drop(country)) %>%
	# Order country factors by their most recent value.
	mutate(country = fct_reorder2(country, date, (low + high) / 2)) %>%

	group_by(fingerprint, transport, country) %>%
	complete(date = seq.Date(min(date), max(date), "days")) %>%
	ungroup()

p <- ggplot(top_countries) +
	# Event annotations.
	geom_vline(data = POINT_EVENTS,
		aes(xintercept = date),
		color = "red",
		alpha = 0.3
	) +
	geom_rect(data = RANGE_EVENTS,
		aes(xmin = begin_date, xmax = end_date + 1, ymin = -Inf, ymax = Inf),
		fill = "red",
		alpha = 0.1
	) +

	# Data series.
	geom_ribbon(
		aes(x = date, ymin = low, ymax = high, fill = country, color = country),
		outline.type = "lower",
		linewidth = LINE_SIZE,
		alpha = 0.9
	) +

	# Text labels.
	geom_text(aes(x = date, y = high, label = country), vjust = -0.25, size = 2) +

	# Separate fingerprints.
	facet_grid(
		rows = vars(fingerprint),
		scales = "free_y",
		labeller = function(x) { list(WANTED_FINGERPRINTS[as.character(x$fingerprint)]) }
	) +

	scale_color_brewer(palette = "Set1") +
	scale_fill_brewer(palette = "Set1") +
	scale_y_continuous(
		limits = c(0, NA),
		labels = scales::label_comma()
	) +
	scale_x_date(
		date_breaks = "1 week",
		date_minor_breaks = "1 day",
		date_labels = "%d %b\n%Y"
	) +
	coord_cartesian(xlim = DATE_LIMITS, expand = FALSE) +
	labs(
		title = sprintf("Top %d countries with the most Snowflake users by day", TOP_N),
		x = NULL,
		y = "Average simultaneous users"
	) +
	guides(fill = guide_legend(override.aes = list(alpha = 1.0, size = 1)), color = NULL)
ggsave(output_path, p, width = 8, height = 4)
