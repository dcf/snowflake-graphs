# Makes a graph showing the goodput bandwidth of the snowflake-01 bridge.
#
# Usage:
#   Rscript bandwidth.r bandwidth-multi.csv snowflake-bandwidth.png

options(tidyverse.quiet = TRUE)
library("tidyverse")

# DATE_LIMITS is defined in common.r.
source("common.r")

LINE_COLOR_PALETTE <- c(
	"5481936581E23D2D178105D44DB6915AB06BFB7F" = "dodgerblue",
	"91DA221A149007D0FD9E5515F5786C3DD07E4BB0" = "navy"
)

(function() {
	args <- commandArgs(trailingOnly = TRUE)
	if (length(args) != 2) {
		stop("usage: Rscript bandwidth.r bandwidth-multi.csv snowflake-bandwidth.png")
	}
	bandwidth_csv_path <<- args[[1]]
	output_path <<- args[[2]]
})()

bandwidth <- read_csv(bandwidth_csv_path,
		comment = "#",
		col_types = cols(
			date          = col_date(),
			fingerprint   = col_factor(),
			type          = col_factor(),
			bytes         = col_double(),
			num_instances = col_double(),
			coverage      = col_double(),
		)
	) %>%

	filter(fingerprint %in% names(WANTED_FINGERPRINTS)) %>%

	group_by(fingerprint, type) %>%
	complete(date = seq.Date(min(date), max(date), "days")) %>%
	ungroup() %>%

	# Compensate for days when not all descriptors were published.
	mutate(bytes = bytes / (coverage / pmax(num_instances, coverage))) %>%

	pivot_wider(id_cols = c(date, fingerprint), names_from = c(type), values_from = c(bytes))

# Comment to show all bridges, not only snowflake-01.
bandwidth <- filter(bandwidth, fingerprint == "5481936581E23D2D178105D44DB6915AB06BFB7F")

p <- ggplot(bandwidth%>%
	filter(lubridate::`%within%`(date, do.call(lubridate::interval, as.list(DATE_LIMITS))))) +
	# Event annotations.
	geom_vline(data = POINT_EVENTS,
		aes(xintercept = date),
		color = "red",
		alpha = 0.3
	) +
	geom_rect(data = RANGE_EVENTS,
		aes(xmin = begin_date, xmax = end_date + 1, ymin = -Inf, ymax = Inf),
		fill = "red",
		alpha = 0.1
	) +

	# Data series.
	geom_line(
		aes(
			x = date,
			# Average read and write bandwidth (which are
			# approximately equal), after subtracting bandwidth
			# used only for answering directory requests.
			y = (read + write - (`dirreq-read` + `dirreq-write`)) / 2,
			color = fingerprint
		)
	) +

	scale_color_manual(
		guide = "none",
		name = "Bridge",
		values = LINE_COLOR_PALETTE,
		labels = WANTED_FINGERPRINTS
	) +

	scale_y_continuous(
		limits = c(0, NA),
		labels = scales::label_bytes()
	) +
	scale_x_date(
		date_breaks = "1 week",
		date_minor_breaks = "1 day",
		date_labels = "%d %b\n%Y"
	) +
	coord_cartesian(xlim = DATE_LIMITS, expand = FALSE) +
	labs(
		title = "Daily Snowflake bandwidth",
		x = NULL,
		y = "Bytes"
	)
ggsave(output_path, p, width = 8, height = 3)
