# Makes a graph showing the estimated number of simultaneous Snowflake users of
# the snowflake-01 bridge.
#
# Usage:
#   Rscript userstats-bridge-transport.r userstats-bridge-transport-multi.csv snowflake-userstats-bridge-transport.png

options(tidyverse.quiet = TRUE)
library("tidyverse")

# DATE_LIMITS is defined in common.r.
source("common.r")

LINE_COLOR_PALETTE <- c(
	"5481936581E23D2D178105D44DB6915AB06BFB7F" = "dodgerblue",
	"91DA221A149007D0FD9E5515F5786C3DD07E4BB0" = "navy"
)

(function() {
	args <- commandArgs(trailingOnly = TRUE)
	if (length(args) != 2) {
		stop("usage: Rscript userstats-bridge-transport.r userstats-bridge-transport-multi.csv snowflake-userstats-bridge-transport.png")
	}
	userstats_bridge_transport_csv_path <<- args[[1]]
	output_path <<- args[[2]]
})()

bridge_transport <- read_csv(userstats_bridge_transport_csv_path,
		comment = "#",
		col_types = cols(
			date          = col_date(),
			fingerprint   = col_factor(),
			transport     = col_factor(),
			users         = col_double(),
			num_instances = col_double(),
			coverage      = col_double(),
		)
	) %>%

	filter(transport == "snowflake" & fingerprint %in% names(WANTED_FINGERPRINTS)) %>%

	group_by(fingerprint, transport) %>%
	complete(date = seq.Date(min(date), max(date), "days")) %>%
	ungroup() %>%

	# Compensate for days when not all descriptors were published.
	mutate(users = users / (coverage / pmax(num_instances, coverage)))

# Comment to show all bridges, not only snowflake-01.
bridge_transport <- filter(bridge_transport, fingerprint == "5481936581E23D2D178105D44DB6915AB06BFB7F")

p <- ggplot(bridge_transport %>%
	filter(lubridate::`%within%`(date, do.call(lubridate::interval, as.list(DATE_LIMITS))))) +
	# Event annotations.
	geom_vline(data = POINT_EVENTS,
		aes(xintercept = date),
		color = "red",
		alpha = 0.3
	) +
	geom_rect(data = RANGE_EVENTS,
		aes(xmin = begin_date, xmax = end_date + 1, ymin = -Inf, ymax = Inf),
		fill = "red",
		alpha = 0.1
	) +

	# Data series.
	geom_line(aes(x = date, y = users, color = fingerprint)) +

	scale_color_manual(
		guide = "none",
		name = "Bridge",
		values = LINE_COLOR_PALETTE,
		labels = WANTED_FINGERPRINTS
	) +

	scale_y_continuous(
		limits = c(0, NA),
		labels = scales::label_comma()
	) +
	scale_x_date(
		date_breaks = "1 week",
		date_minor_breaks = "1 day",
		date_labels = "%d %b\n%Y"
	) +
	coord_cartesian(xlim = DATE_LIMITS, expand = FALSE) +
	labs(
		title = "Daily Snowflake users",
		x = NULL,
		y = "Average simultaneous users"
	)
ggsave(output_path, p, width = 8, height = 3)
