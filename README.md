Scripts for generating graphs related to Snowflake and Snowflake bridges.

Special scripts for Snowflake were initially required
because of the unusual [multi-process architecture](https://gitlab.torproject.org/tpo/anti-censorship/team/-/wikis/Survival%20Guides/Snowflake%20Bridge%20Installation%20Guide#introduction)
used on the Snowflake bridges to enable scaling.
The multiple instances of tor on each bridge publish multiple
[bridge-extra-info](https://metrics.torproject.org/collector.html#type-bridge-extra-info) descriptors,
all having the same relay fingerprint (but with different nicknames).
Before October 2022, Tor Metrics
[was not equipped](https://gitlab.torproject.org/tpo/network-health/metrics/website/-/issues/40047)
to understand this deployment model:
it considered the relay fingerprint to be identical with a single instance of tor.
Multiple instances of tor with the same fingerprint
would end up overwriting each others' descriptors,
and so Tor Metrics effectively reported numbers that were 1/<var>N</var> of their true values.
Special graph-making scripts were required, then,
to treat descriptors with different (<var>fingerprint</var>, <var>nickname</var>) pairs
as belonging to different instances of tor.

Now, Tor Metrics knows about how Snowflake bridges are deployed
and takes into account all <var>N</var> instances.
Custom graph scripts are still useful
for being able to separate out the shares of different bridges,
or get a breakdown of Snowflake users by country.

There is one way in which the user count estimates computed by these scripts
differs from Tor Metrics:
it does not adjust the counts according to the [`frac`](https://metrics.torproject.org/reproducible-metrics.html#users)
estimated fraction of reported directory-request statistics.
They instead hardcode `frac` = 1.0,
which will tend to make the counts slightly lower than what Tor Metrics reports.
Computing `frac` is difficult and requires processing even non-Snowflake descriptors;
but moreover, it is [not appropriate for Snowflake](https://lists.torproject.org/pipermail/tor-dev/2022-April/014725.html),
where there are only few bridges that are all well known
to report directory request statistics.

Related links:

* ["Here's my own graph of write history, with 'duplicate' fingerprints distinguished by nickname"](https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40095#note_2773395)
* ["I did some analysis to see what a corrected graph for Snowflake would look like"](https://gitlab.torproject.org/tpo/network-health/metrics/website/-/issues/40047#note_2796619)
* ["This graph shows shows all the Tor users in Russia, whether using relays, bridges, or bridges with pluggable transports"](https://gitlab.torproject.org/tpo/community/support/-/issues/40050#note_2796770)
* [Graphs of user counts from Iran since the onset of shutdowns](https://forum.torproject.net/t/graphs-of-user-counts-from-iran-since-the-onset-of-shutdowns/4843)


## How to make users and bandwidth graphs

Install required (Debian) packages:

    sudo apt install make python3 python3-stem python3-pandas r-base-core r-cran-tidyverse

Download bridge-extra-info descriptors from

    https://metrics.torproject.org/collector/archive/bridge-descriptors/extra-infos/

Try this (using the Collector [index.json file](https://metrics.torproject.org/collector.html#index-json)):

    wget -O - https://collector.torproject.org/index/index.json | jq -r '.directories[]|select(.path == "archive").directories[]|select(.path == "bridge-descriptors").directories[]|select(.path == "extra-infos").files[]|select(.first_published >= "2017")|("https://collector.torproject.org/archive/bridge-descriptors/extra-infos/"+.path)' | wget --input-file=- --timestamping

Type `make -j4`.
(You can use a higher degree of parallelism with e.g. `-j8`.)

Several .csv files will be created for each input bridge-extra-infos-\*.tar.xz file,
and these will be aggregated into files:
userstats-bridge-transport-multi.csv,
userstats-bridge-combined-multi.csv,
bandwidth-multi.csv.

Edit `DATE_LIMITS` in common.r. Run

    Rscript userstats-bridge-transport.r userstats-bridge-transport-multi.csv snowflake-userstats-bridge-transport.png
    Rscript top-countries.r userstats-bridge-combined-multi.csv snowflake-top-countries.png
    Rscript bandwidth.r bandwidth-multi.csv snowflake-bandwidth.png


## How to make proxy graphs

Download snowflake-stats descriptors from

    https://metrics.torproject.org/collector/archive/snowflakes/

Try this:

    wget -O - https://collector.torproject.org/index/index.json | jq -r '.directories[]|select(.path == "archive").directories[]|select(.path == "snowflakes").files[]|("https://collector.torproject.org/archive/snowflakes/"+.path)' | wget --input-file=- --timestamping

Type `make -j4`.
(You can use a higher degree of parallelism with e.g. `-j8`.)

Several .csv files will be created for each input snowflakes-\*.tar.xz file,
and these will be aggregated into files:
proxy-country.csv,
proxy-nat-type.csv,
proxy-type.csv,
client-match.csv.
