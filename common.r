# Definitions common to multiple graph programs.

DATE_LIMITS <- lubridate::ymd(c(
	"2024-12-25",
	"2025-02-01"
))

WANTED_FINGERPRINTS <- c(
	"7659DA0F96B156C322FBFF3ACCC9B9DC01C27C73" = "snowman",
	"5481936581E23D2D178105D44DB6915AB06BFB7F" = "snowflake-01",
	"91DA221A149007D0FD9E5515F5786C3DD07E4BB0" = "snowflake-02"
)

POINT_EVENTS <- tribble(
	~date, ~label,
	# "2021-07-06 16:56:37", "Tor Browser 10.5\nincludes Snowflake",     # https://blog.torproject.org/new-release-tor-browser-105
	"2021-12-01 00:00:00", "Tor blocking in Russia",                     # https://bugs.torproject.org/tpo/community/support/40050
	# "2021-12-14 00:00:00", "Tor Browser 11.5a1 with DTLS fix",           # https://blog.torproject.org/new-release-tor-browser-115a1/
	"2021-12-20 00:00:00", "Tor Browser 11.0.3 with DTLS fix",           # https://blog.torproject.org/new-release-tor-browser-1103/
	"2022-01-25 17:41:00", "Load balancing",                             # https://bugs.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/40095#note_2772325
	"2022-03-16 16:51:35", "Bridge hardware upgrade",                    # https://bugs.torproject.org/tpo/tpa/team/40664#note_2787624
	# "2022-04-11 15:50:00", "snowflake-01 changeover",                    # https://bugs.torproject.org/tpo/tpa/team/40716#note_2794856
	# "2022-07-14 00:00:00", "Tor Browser 11.5 automatic configuration",   # https://blog.torproject.org/new-release-tor-browser-115/
	"2022-09-20 00:00:00", "Protests in Iran",                           # https://lists.torproject.org/pipermail/anti-censorship-team/2022-September/000247.html
	"2022-10-06 00:00:00", "TLS fingerprint blocking in Iran",           # https://bugs.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/40207#note_2849437
	# "2022-10-27 00:00:00", "Tor Browser 11.5.6 with TLS fix",            # https://blog.torproject.org/new-release-tor-browser-1156/
	"2022-11-01 00:00:00", "Orbot 16.6.3-RC-1-tor.0.4.7.10 with TLS fix",# https://github.com/guardianproject/orbot/releases/tag/16.6.3-RC-1-tor.0.4.7.10
	# "2023-02-15 00:00:00", "Tor Browser 12.0.3 with DTLS fix",           # https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/merge_requests/637
	# "2023-02-16 21:16:47", "Restart snowflake-02 for haproxy security update", # https://bugs.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/40253#note_2879465
	"2023-02-17 03:04:19", "Restart snowflake-01 for haproxy security update", # https://bugs.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/40253#note_2879511
	# "2023-03-13 19:46:07", "Restart snowflake-02 for QueuePacketConn fix", # https://bugs.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/40262#note_2886032
	"2023-03-13 20:17:54", "Restart snowflake-01 for QueuePacketConn fix", # https://bugs.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/40262#note_2886041
	"2024-03-01 00:00:00", "Fastly stops supporting domain fronting",    # https://bugs.torproject.org/tpo/anti-censorship/team/135
) %>% mutate(date = lubridate::ymd_hms(date) %>% lubridate::as_date())

RANGE_EVENTS <- tribble(
	~begin_date, ~end_date, ~label,
	"2023-01-16 00:00:00", "2023-01-24 00:00:00", "Default front domain blocked in Iran", # https://bugs.torproject.org/tpo/anti-censorship/team/115#note_2873040
	"2023-01-31 00:00:00", "2023-02-01 00:00:00", "Default front domain blocked in Iran", # https://bugs.torproject.org/tpo/anti-censorship/team/115#note_2876012
	"2023-02-08 00:00:00", "2023-02-13 00:00:00", "Default front domain blocked in Iran", # https://explorer.ooni.org/chart/mat?probe_cc=IR&since=2023-01-23&until=2023-03-01&time_grain=day&axis_x=measurement_start_day&test_name=web_connectivity&domain=cdn.sstatic.net
	"2023-02-19 00:00:00", "2023-02-19 00:00:00", "Default front domain blocked in Iran", # https://explorer.ooni.org/chart/mat?probe_cc=IR&since=2023-01-23&until=2023-03-01&time_grain=day&axis_x=measurement_start_day&test_name=web_connectivity&domain=cdn.sstatic.net
	"2023-02-22 00:00:00", "2023-02-22 00:00:00", "Default front domain blocked in Iran", # https://explorer.ooni.org/chart/mat?probe_cc=IR&since=2023-01-23&until=2023-03-01&time_grain=day&axis_x=measurement_start_day&test_name=web_connectivity&domain=cdn.sstatic.net
	"2023-03-03 00:00:00", "2023-03-03 00:00:00", "Default front domain blocked in Iran", # https://explorer.ooni.org/chart/mat?probe_cc=IR&since=2023-02-24&until=2023-04-04&time_grain=day&axis_x=measurement_start_day&test_name=web_connectivity&domain=cdn.sstatic.net
	"2023-03-08 00:00:00", "2023-03-08 00:00:00", "Default front domain blocked in Iran", # https://explorer.ooni.org/chart/mat?probe_cc=IR&since=2023-02-24&until=2023-04-04&time_grain=day&axis_x=measurement_start_day&test_name=web_connectivity&domain=cdn.sstatic.net
	"2023-03-13 00:00:00", "2023-03-13 00:00:00", "Default front domain blocked in Iran", # https://explorer.ooni.org/chart/mat?probe_cc=IR&since=2023-02-24&until=2023-04-04&time_grain=day&axis_x=measurement_start_day&test_name=web_connectivity&domain=cdn.sstatic.net
	"2023-03-19 00:00:00", "2023-03-19 00:00:00", "Default front domain blocked in Iran"  # https://explorer.ooni.org/chart/mat?probe_cc=IR&since=2023-02-24&until=2023-04-04&time_grain=day&axis_x=measurement_start_day&test_name=web_connectivity&domain=cdn.sstatic.net
) %>% mutate(
	begin_date = lubridate::ymd_hms(begin_date) %>% lubridate::as_date(),
	end_date   = lubridate::ymd_hms(end_date)   %>% lubridate::as_date()
)

# Return an abbreviation for the month, followed by a year for January only.
date_labels <- function(breaks) {
	mon <- strftime(breaks, "%b")
	year <- strftime(breaks, "%Y")
	ifelse(!is.na(breaks) & lubridate::month(breaks) == 1, paste(mon, year, sep = "\n"), mon)
}

theme_set(
	theme_minimal() +
	theme(plot.background = element_rect(fill = "white", color = NA, linewidth = 0))
)
update_geom_defaults("line", aes(linewidth = 0.4))
