#!/usr/bin/env python3

# Parses proxy_country CSV files produced by the snowflake-stats program,
# deduplicates rows, sums them by date and type, and writes a CSV file.
#
# Usage:
#   proxy-country proxy_country/snowflakes-*.proxy_country.csv > proxy-country.csv

import getopt
import os
import sys

import numpy as np
import pandas as pd

import common

def usage(f = sys.stdout):
    print(f"""\
Usage:
  {sys.argv[0]} [OPTION...] proxy_country/snowflakes-*.proxy_country.csv > proxy-country.csv

Options:
  -h, --help            show this help
""", file = f)

if __name__ == "__main__":
    opts, input_paths = getopt.gnu_getopt(sys.argv[1:], "h", [
        "help",
    ])
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit(0)

    # Keep only one copy per unique "begin", "end", and "country". In practice,
    # we don't expect this to have any effect, because snowflake-stats
    # descriptors do not have information that partially overlaps with other
    # snowflake-stats descriptors, the way bridge-extra-info descriptors do.
    proxy_country_stats = (
        pd.concat(pd.read_csv(path, parse_dates = ["begin", "end"]) for path in input_paths)
        .sort_values(["end", "unique_ips"]) # Keep the one with the highest unique_ips.
        .groupby(["begin", "end", "country"], dropna = False)
        .last()
        .reset_index()
    )

    # Distribute over dates.
    proxy_country_stats_bydate = {
        "date": [],
        "country": [],
        "unique_ips": [],
        "coverage": [],
    }
    for row in proxy_country_stats.itertuples():
        for (date, frac_int, frac_day) in common.segment_datetime_interval(row.begin, row.end):
            proxy_country_stats_bydate["date"].append(date)
            proxy_country_stats_bydate["country"].append(row.country)
            proxy_country_stats_bydate["unique_ips"].append(row.unique_ips * frac_int)
            proxy_country_stats_bydate["coverage"].append(frac_day)
    proxy_country_stats_bydate = (
        pd.DataFrame(proxy_country_stats_bydate)
            .groupby(["date", "country"], dropna = False)
            .sum()
            .reset_index()
    )

    (
        proxy_country_stats_bydate
        # Sort for a more readable CSV.
        .sort_values(["date", "country"])
    ).to_csv(sys.stdout, index = False, float_format = "%.2f", columns = [
        "date",
        "country",
        "unique_ips",
        "coverage",
    ])
